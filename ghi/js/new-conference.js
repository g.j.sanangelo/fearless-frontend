window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      const data = await response.json();
      const selectTag = document.getElementById('location');

      for (let location of data.locations) {
        const option = document.createElement('option');
        option.value = location.id;
        option.innerHTML = location.name;
        selectTag.appendChild(option);
      }

      const formTag = document.getElementById('create-conference-form');
      formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: 'POST',
          body: json,
          headers: {
            'Content-Type': 'application/json',
          },
        };

        try {
          const response = await fetch(conferenceUrl, fetchConfig);

          if (!response.ok) {
            throw new Error('Network response was not ok');
          }

          const newConference = await response.json();
          console.log('New conference created:', newConference);

          // Reset the form after successful submission
          formTag.reset();
        } catch (error) {
          console.error('There was a problem with the fetch operation:', error);
        }
      });
    } catch (error) {
      console.error('There was a problem loading locations:', error);
    }
  });
