function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <a className="navbar-brand" href="/">Conference Go!</a>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="http://localhost:3000/">Home</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="http://localhost:3000/new-location.html">New location</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/contact">New conference</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/contact">New presentation</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }

  export default Nav;
